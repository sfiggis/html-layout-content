# Layout a Resume #

Using HTML to create and layout a resume page.

### Clone Repository ###

1. Fork this repository.
2. Git clone your own copy to your assignments folder.
3. Change directory into your local copy of the repo.

### To Do ###

1. Open resume.html in Sublime and have a look at its structure.
2. Move a picture of yourself into your local folder, change the resume picture.
3. Add details to your resume page, add elements as and when needed.
4. Use your new knowledge and skills to layout your page like a resume should be.
5. You can see your progress by typing in terminal 'open resume.html'.